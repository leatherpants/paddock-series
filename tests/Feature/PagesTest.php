<?php

namespace Tests\Feature;

use Tests\TestCase;

class PagesTest extends TestCase
{
    /** @test */
    public function test_shows_landing_page()
    {
        $response = $this->get(route('index'));
        $response->assertSuccessful();
    }
}
