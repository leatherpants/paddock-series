<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;
use App\paddock\Teams\Models\Teams;

class TeamsTest extends TestCase
{
    /** @test */
    public function test_get_teams_list()
    {
        $teams = factory(Teams::class)->create();

        $data = [
            'teams' => $teams,
        ];

        $response = $this->actingAs($this->users)->get(route('backend.teams'), $data);
        $response->assertSuccessful();
    }

    /** @test */
    public function test_shows_team_form_for_creation()
    {
        $response = $this->actingAs($this->users)->get(route('backend.teams.create'));
        $response->assertSuccessful();
    }

    /** @test */
    public function test_store_new_teams()
    {
        $data = [
            'name' => 'Ferrari',
            'slug' => 'ferrari',
            'full_name' => 'Scuderia Ferrari',
        ];

        $response = $this->actingAs($this->users)->post(route('backend.teams.create'), $data);
        $response->assertStatus(302);
        $response->assertRedirect(route('backend.teams'));
    }
}
