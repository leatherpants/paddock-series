<?php

namespace Tests\Feature\Backend;

use Tests\TestCase;

class HomeTest extends TestCase
{
    /** @test */
    public function test_shows_dashboard()
    {
        $response = $this->get(route('backend'));
        $response->assertSuccessful();
    }
}
