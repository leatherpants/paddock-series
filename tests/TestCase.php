<?php

namespace Tests;

use App\paddock\Teams\Models\Teams;
use App\paddock\Users\Models\Users;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseMigrations;

    protected $users;
    protected $teams;

    /**
     *  Set up the test.
     */
    public function setUp()
    {
        parent::setUp();

        $this->users = factory(Users::class)->create();
        $this->teams = factory(Teams::class)->create();
    }

    public function tearDown()
    {
        $this->artisan('migrate:reset');
        parent::tearDown();
    }
}
