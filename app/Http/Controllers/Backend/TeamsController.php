<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\paddock\Teams\Repository\TeamsRepository;
use App\paddock\Teams\Requests\CreateTeamsRequest;

class TeamsController extends Controller
{
    /**
     * @var TeamsRepository
     */
    private $teamsRepository;

    /**
     * TeamsController constructor.
     * @param TeamsRepository $teamsRepository
     */
    public function __construct(TeamsRepository $teamsRepository)
    {
        $this->teamsRepository = $teamsRepository;
    }

    /**
     * Get teams list.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $teams = $this->teamsRepository->list();

        return view('backend.teams.index')
            ->with('teams', $teams);
    }

    /**
     * Shows the form for creating new teams.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('backend.teams.create');
    }

    /**
     * Store new teams.
     * @param CreateTeamsRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(CreateTeamsRequest $request)
    {
        $data = [
            'name' => $request->input('name'),
            'slug' => str_slug($request->input('name')),
            'full_name' => $request->input('full_name'),
        ];

        $this->teamsRepository->create($data);

        return redirect()
            ->route('backend.teams');
    }
}
