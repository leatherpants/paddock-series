<?php

namespace App\paddock\Teams\Repository;

use App\paddock\Teams\Models\Teams;

class TeamsRepository
{
    /**
     * @var Teams
     */
    private $teams;

    /**
     * TeamsRepository constructor.
     * @param Teams $teams
     */
    public function __construct(Teams $teams)
    {
        $this->teams = $teams;
    }

    /**
     * Create new teams.
     * @param array $data
     */
    public function create($data)
    {
        $teams = new Teams();
        $teams->name = $data['name'];
        $teams->slug = $data['slug'];
        $teams->full_name = $data['full_name'];
        $teams->save();
    }

    /**
     * Get teams list.
     * @return mixed
     */
    public function list()
    {
        return $this->teams
            ->get();
    }
}
