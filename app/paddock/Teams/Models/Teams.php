<?php

namespace App\paddock\Teams\Models;

use Illuminate\Database\Eloquent\Model;

class Teams extends Model
{
    /**
     * The attributes that are mass assignable.
     * @var array
     */
    protected $fillable = [
        'name',
        'slug',
        'full_name',
    ];
}
