<?php

use Illuminate\Support\Facades\Route;

Route::group(['namespace' => 'Backend', 'prefix' => 'backend'], function () {
    Route::get('', ['uses' => 'HomeController@index', 'as' => 'backend']);

    Route::group(['prefix' => 'teams'], function () {
        Route::get('', ['uses' => 'TeamsController@index', 'as' => 'backend.teams']);
        Route::get('create', ['uses' => 'TeamsController@create', 'as' => 'backend.teams.create']);
        Route::post('create', ['uses' => 'TeamsController@store']);
        Route::get('{id}/edit', ['uses' => 'TeamsController@edit', 'as' => 'backend.teams.edit']);
        Route::post('{id}/edit', ['uses' => 'TeamsController@update']);
        Route::get('{id}/destroy', ['uses' => 'TeamsController@destroy', 'as' => 'backend.teams.destroy']);
        Route::get('{id}/show', ['uses' => 'TeamsController@show', 'as' => 'backend.teams.show']);
    });
});

Route::group(['namespace' => 'Frontend'], function () {
    Route::get('', ['uses' => 'HomeController@index', 'as' => 'index']);
});
