<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Teams\Models\Teams::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'slug' => $faker->slug,
        'full_name' => $faker->name,
    ];
});
