<?php

use Faker\Generator as Faker;

$factory->define(\App\paddock\Users\Models\Users::class, function (Faker $faker) {
    return [
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});
